# Seal the Deal

<img src="./logo.png"
     alt="Seal the Deal Logo"
     style="float: right; margin-right: 10px;
     margin-top: -150px;"
     width="230px">

## The most awesome game of alltime

___

### Installation and Run Setup

``` bash
# Install dependencies
npm install

# Run with hot reload at localhost:5000
# Initializes server with NODE_ENV = 'development'
npm run dev

# Run in production
# Initializes server with NODE_ENV = 'production'
npm run start

# Run unit tests
npm run test

```

____

### The team

* André Ribeiro
* Edna Martins
* João Raimundo
* Patrícia Ribeiro
