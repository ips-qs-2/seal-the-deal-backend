/**
 * @module index
 * Server entry point
 *      Initialization of HTTP and WebSocket Servers
 *
 * @package root
 *
 * @author Seal the Deal - Team
 * @version 1.0.1
 * @date 2020/11/28
 * @lastrevision
 *
 */
require('dotenv').config();

// Get the first argument after command name and file name
// if its either development or production.
// Otherwise is set to null.
const firstArg = process.argv.slice(2).length > 0 ? process.argv.slice(2)[0].toLowerCase() : null;
const argNodeEnd = firstArg === 'development' || firstArg === 'production' ? firstArg : null;

// Import external modules
const express = require('express');
const app = express();
const bodyParser = require('body-parser');

// Import internal modules
const util = require('./util');
const httpApi = require('./apis/http-api.js');
const wsWrapper = require('./connectors/ws-wrapper');
const manager = require('./managers/index-manager');

// Global Constants declaration
const config = require('./config');
const NODE_ENV = argNodeEnd || process.env.NODE_ENV || 'development';
const PORT = process.env.PORT || config.server.port;
const CORE_WS_SERVER = process.env.CORE_WS_SERVER;

// Disable console logging on production mode
if (NODE_ENV !== 'development') console.log = function () {};

// HTTP request logging in development mode
if (NODE_ENV === 'development') app.use('*', util.exposeRequest);

// Express configuration
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.text({ defaultCharset: 'utf-8' }));
app.use(bodyParser.json());
app.use('/api', httpApi);

// Start HTTP Express Server
const server = app.listen(PORT, () => {
    const host = server.address().address === '::' ? 'localhost' : server.address().address;
    console.log('\x1b[35m%s\x1b[0m', `${NODE_ENV.toUpperCase()} mode`);
    console.log('\x1b[36m%s\x1b[0m', `Listening at http://${host}:${PORT}/`);
    util.writeLogText('Restarted server');
});

// Start game WebSocket Server
const wsServer = new wsWrapper.MyWebSocket(null, server);

// Start core WebSocket client
const wsCore = new wsWrapper.MyWebSocket({ url: 'ws://' + CORE_WS_SERVER }, null);

// Start Game Server Maestro
const maestro = new manager.Maestro(wsServer, wsCore);

app.use('/peek/clients', (req, res) => {
    res.json(maestro.peekClients());
});

app.use('/peek/games', (req, res) => {
    res.json(maestro.peekGames());
});

app.use('/peek/logs', (req, res) => {
    util.getLog((err, result) => {
        if (err) return res.send(err);
        res.send(result);
    });
});

module.exports = app;
