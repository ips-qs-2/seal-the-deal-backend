// Import modules
const chai = require('chai');
const assert = chai.assert;
const expect = require('chai').expect;
const chaiHttp = require('chai-http');
const WebSocket = require('ws');
const util = require('./util');
const config = require('./config.json');
const Player = require('./models/player');
const playerController = require('./controllers/player-controller');
const Game = require('./models/game');

// Service to test
const server = require('./index');

chai.use(chaiHttp);
chai.should();

describe('A testar servidor HTTP', () => {
    describe('GET Ping!', () => {
        it('/api/ping > Deve retornar 200 OK com mensagem Pong! Pang!', (done) => {
            chai.request(server)
                .get('/api/ping')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.text.should.be.equal('Pong! Pang!');
                    done();
                });
        });
    });
});

describe('A testar servidor WebSockets', () => {
    const wss = new WebSocket(`ws://localhost:${config.server.port}/test/`);
    const responses = { command: null, echo: null };
    const testWssMessage = { command: 'ok', echo: { command: 'testWebsocketServer' } };

    wss.on('open', function open() {
        wss.send(JSON.stringify({ command: 'testWebsocketServer' }));
    });

    wss.on('close', function close() {});

    wss.on('message', function incoming(message) {
        message = util.getJson(message);
        responses.command = message.command;
        responses.echo = message.echo;
    });

    after((done) => {
        wss.terminate();
        done();
    });

    it('Send echo request, expect same message', (done) => {
        expect(responses.command).to.be.equal(testWssMessage.command);
        expect(responses.echo.command).to.be.equal(testWssMessage.echo.command);
        done();
    });
});

describe('Testing function soma()', () => {
    //test a function for a specific case
    it('should return 4 when add 2 + 2', () => {
        const result = util.soma(2, 2);
        const expectedResult = 4;
        assert.equal(result, expectedResult);
    });

    it('should return -2 when add -4 + 2', () => {
        const result = util.soma(-4, 2);
        const expectedResult = -2;
        assert.equal(result, expectedResult);
    });

    it('should not return 5 when add 2 + 2', () => {
        const result = util.soma(2, 2);
        const expectedResult = 5;
        assert.notEqual(result, expectedResult);
    });
});

// Testar Player e Game

//Plano de testes, Testar o crud do utilizador:: Criar, editar eliminar listar,
const testPlayer = new Player('id_teste_2', 'Teste2', '2021-02-08', 'a@b.pt', 'avatar');

describe('New player -  ', () => {
    //test a function for a specific case
    it('Adcionar um jogador com id: ' + testPlayer.id, (done) => {
        testPlayer.save((err, result) => {
            if (err) return;
            expect(result.upsertedId).to.equal(testPlayer.id);
            done();
        });
    });
    it('Verificar se Jogador foi adicionado com o id:' + testPlayer.id, (done) => {
        Player.one(testPlayer.id, (err, result) => {
            if (err) return;
            // Comprar dois objectos (eql)
            expect(result).to.eql(testPlayer);
            done();
        });
    });
});

describe('Eliminar - Quantidade de jogadores ', () => {
    it('Eliminar jogador com id:', (done) => {
        // Eliminar
        Player.one(testPlayer.id, (err, result) => {
            if (err) return;
            const resultdelete = result.delete();
            const resultExpetec = true;
            assert.notEqual(resultdelete, resultExpetec);
            done();
        });
    });
    it('Verificar a Quantidade de jogadores cadastrados ', (done) => {
        Player.many({}, (err, result) => {
            if (err) return;
            assert.notEqual(result.length, 0);
            done();
        });
    });
});
const playerOne = {
    name: 'Ema Barão',
    birthday: '1990-10-01',
    email: 'qspai-g4@gmail.com',
    avatar: 'https://i.imgur.com/CqgF3HB.png',
    mode: 'easy',
    sessionId: 'emaB',
    lifes: 1,
    turnToPlay: false,
    xPos: '10',
    yPos: '8',
};
const playerTwo = {
    name: 'patixa1',
    birthday: '2001-01-01',
    email: 'patixa1@somemail.com',
    avatar: '(blob)',
    mode: 'hard',
    sessionId: 'patixa1',
    lifes: 1,
    turnToPlay: false,
    xPos: '10',
    yPos: '8',
};
const startedTS = 1613221553576;
const endedTS = 1613221610342;
const gameresult = [1, -1];
const novojogo = new Game(playerOne, playerTwo, startedTS, endedTS, gameresult);

describe('Jogo', () => {
    it('Novo Jogo  ' + testPlayer.id, (done) => {
        novojogo.save((err, insertresult) => {
            if (err) {
                return;
            }
            resultExpetec = true;
            expect(insertresult).to.equal(resultExpetec);
            done();
        });
    });
});
