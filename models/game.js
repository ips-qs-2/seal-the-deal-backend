/**
 * @class Game
 *  Provides abstraction to deal with databse
 *
 * @package models
 *
 * @returns following methods:
 * @static one, many
 * @public save, delete
 * @public getters and setters
 *
 * @author Seal the Deal - Team
 * @version 1.0.1
 * @date 2020/11/28
 * @lastrevision
 *
 */

const db = require('../connectors/mongo-wrapper');
const { ObjectID } = require('mongodb');
const dbCollection = require('../config.json').database.collections.games;

/**
 *
 */
class Game {
    /**
     * Construtor da class Game
     * @public
     * @param {object} playerOne : { playerOneId, mode}
     * @param {object} playerTwo : { playerTwoId, mode}
     * @param {timestamp} startedTS
     * @param {timestamp} endedTS
     * @param {integer} result : 0: draw, 1: playerOne won, 2: playerTwo won
     */
    constructor(playerOne, playerTwo, startedTS, endedTS, result) {
        this.playerOne = playerOne;
        this.playerTwo = playerTwo;
        this.startedTS = startedTS;
        this.endedTS = endedTS;
        this.result = result;
        Object.defineProperty(this, '_id', { enumerable: true, writable: true });
    }

    // Database operations
    /**
     * Get all
     * @param {*} callback
     */
    static many(query, callback) {
        return db.findMany(query, dbCollection, (err, rows) => {
            if (err) return callback(err, null);
            rows = rows.map((row) => {
                if (!!row._id) {
                    const game = new this(row.playerOne, row.playerTwo, row.startedTS, row.endedTS, row.result);
                    game.id = row._id;
                    return game;
                }
            });
            callback(null, rows);
        });
    }

    /**
     * Get many
     * @param {*} callback
     */
    static one(id, callback) {
        return db.findOne(
            { filter: { _id: ObjectID(id) } },
            dbCollection,
            (err, { _id, playerOne, playerTwo, startedTS, endedTS, result }) => {
                if (err) return callback(err, null);
                let game = null;
                if (!!_id) {
                    game = new this(playerOne, playerTwo, startedTS, endedTS, result);
                    game.id = _id;
                }
                callback(null, game);
            }
        );
    }

    /**
     * Insert or update this instance in the database
     */
    save(callback) {
        if (this._id === undefined)
            return db.insert(
                {
                    _id: this._id,
                    playerOne: this.playerOne,
                    playerTwo: this.playerTwo,
                    startedTS: this.startedTS,
                    endedTS: this.endedTS,
                    result: this.result,
                },
                dbCollection,
                (err, result) => {
                    if (err) return callback(err, null);
                    this._id = result.insertedId;
                    return callback(null, true);
                }
            );
        else
            return db.update(
                {
                    filter: { _id: ObjectID(this._id) },
                    set: {
                        playerOne: this.playerOne,
                        playerTwo: this.playerTwo,
                        startedTS: this.startedTS,
                        endedTS: this.endedTS,
                        result: this.result,
                    },
                },
                dbCollection,
                null
            );
    }

    delete() {
        if (this._id === undefined) return;
        db.delete({ filter: { _id: this._id } }, dbCollection, (error, result) => {
            if (error) return false;
            return true;
        });
    }
}
module.exports = Game;
