/**
 * @class Player
 *  Provides abstraction to deal with databse
 *
 * @package models
 *
 * @returns following methods:
 * @static one, many
 * @public save, delete
 * @public getters and setters
 *
 * @author Seal the Deal - Team
 * @version 1.0.1
 * @date 2020/11/28
 * @lastrevision
 *
 */
// Import modules
const db = require('../connectors/mongo-wrapper');
const dbCollection = require('../config.json').database.collections.players;

/**
 *
 */
class Player {
    /**
     * Construtor da class Player
     * @public
     * @param {string} name
     * @param {string} birthday
     * @param {string} email
     * @param {blob} avatar
     * @param {integer} points
     * @param {integer} credits
     * @param {object} powerups
     * @param {socket} socket
     */
    constructor(id, name, birthday, email, avatar, points, credits, powerups, socket) {
        const emptyPowers = { seal: 0, speed: 0, scent: 0, radar: 0 };
        this.name = name;
        this.birthday = birthday;
        this.email = email;
        this.avatar = avatar;
        Object.defineProperty(this, 'id', { value: id, enumerable: false, writable: false });
        Object.defineProperty(this, 'credits', { value: credits || 0, enumerable: false, writable: true });
        Object.defineProperty(this, 'points', { value: points || 0, enumerable: false, writable: true });
        Object.defineProperty(this, 'powerups', { value: powerups || emptyPowers, enumerable: false, writable: true });
        Object.defineProperty(this, 'socket', { value: socket || null, enumerable: false, writable: true });
    }

    // Database operations
    /**
     * Get all
     * @param {function} callback
     */
    static many(query, callback) {
        return db.findMany(query, dbCollection, (err, rows) => {
            if (err) return callback(err, null);
            rows = rows.map((row) => {
                const player = new this(row._id, row.name, row.birthday, row.email, row.avatar, row.points);
                player._points = row.points;
                return player;
            });
            callback(null, rows);
        });
    }
    /**
     * Get all
     * @param {function} callback
     */
    static ranking(query, callback) {
        return db.findMany(query, dbCollection, (err, rows) => {
            if (err) return callback(err, null);
            rows = rows.map((row) => {
                const player = new this(row._id, row.name, row.birthday, row.email, row.avatar, row.points);
                player._points = row.points;
                return player;
            });
            callback(null, rows);
        });
    }

    /**
     * Get many
     * @param {funtion} callback
     */
    static one(id, callback) {
        return db.findOne({ filter: { _id: id } }, dbCollection, (err, row) => {
            if (err) return callback(err, null);
            const player = new this(
                row._id,
                row.name,
                row.birthday,
                row.email,
                row.avatar,
                row.points,
                row.credits,
                row.powerups
            );
            callback(null, player);
        });
    }

    /**
     * Save player
     * @param {function} callback
     */
    save(callback) {
        if (typeof callback !== 'function') callback = (...arg) => {};
        const data = {};
        Object.entries(this).forEach(([k, v]) => {
            if (v) data[k] = v;
        });
        // delete data.id;
        db.update(
            {
                filter: { _id: this.id },
                set: data,
                opts: { upsert: true },
            },
            dbCollection,
            (error, result) => {
                if (error) return callback(error, null);
                return callback(null, result);
            }
        );
    }

    /**
     * Update points value
     * @param {integer} credits
     * @param {function} callback
     */
    savePoints(points, callback) {
        if (!points) return false;
        if (typeof callback !== 'function') callback = (...arg) => {};
        const initPoints = this.points;
        const restorePoints = () => {
            this.points = initPoints;
        };
        this.points = points;
        const self = this;
        db.update(
            {
                filter: { _id: self.id },
                set: {
                    points: self.points,
                },
                opts: { upsert: true },
            },
            dbCollection,
            (error, result) => {
                if (error) {
                    restorePoints();
                    return callback(error, null);
                }
                if (result.modifiedCount === 1) return callback(null, { result, self });
            }
        );
    }

    /**
     * Update credits value
     * @param {integer} credits
     * @param {function} callback
     */
    saveCredits(credits, callback) {
        if (!credits) return false;
        if (typeof callback !== 'function') callback = (...arg) => {};
        const initCredits = this.credits;
        const restoreCredits = () => {
            this.credits = initCredits;
        };
        this.credits = credits;
        const self = this;
        db.update(
            {
                filter: { _id: self.id },
                set: {
                    credits: self.credits,
                },
                opts: { upsert: true },
            },
            dbCollection,
            (error, result) => {
                if (error) {
                    restoreCredits();
                    return callback(error, null);
                }
                if (result.modifiedCount === 1) callback(null, { result, self });
            }
        );
    }

    /**
     * Update powerups value
     * @param {integer} powerups
     * @param {function} callback
     */
    savePowerups(powerups, callback) {
        if (!powerups) return false;
        if (typeof callback !== 'function') callback = (...arg) => {};
        const initPowerups = this.powerups;
        const restorePowerups = () => {
            this.powerups = initPowerups;
        };
        this.powerups = powerups;
        const self = this;
        db.update(
            {
                filter: { _id: self.id },
                set: {
                    powerups: self.powerups,
                },
                opts: { upsert: true },
            },
            dbCollection,
            (error, result) => {
                if (error) {
                    restorePowerups();
                    return callback(error, null);
                }
                if (result.modifiedCount === 1) callback(null, { result, self });
            }
        );
    }

    /**
     * Delete player
     */
    delete() {
        if (this.id === undefined) return;
        db.delete({ filter: { _id: this.id } }, dbCollection, (error, result) => {
            if (error) return false;
            return true;
        });
    }
}
module.exports = Player;
