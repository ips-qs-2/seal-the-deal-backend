/**
 * @module ws-connector
 * Module exports options to connect and register WebSockets events and functions
 * Server works based on express
 *
 * @package connectors
 *
 *
 * @author Seal the Deal - Team
 * @version 1.0.1
 * @date 2020/11/28
 * @lastrevision
 *
 */

// Initialize modules and global constants
require('dotenv').config();
const util = require('../util');
const express = require('express');
const WebSocket = require('ws');
// const ReconnectingWebsocket = Object.create(require('reconnecting-websocket'));
const config = require('../config.json');

/**
 * Initializes a new websocket server or client
 *
 * @public
 * @param {object} server :
 *                        : for server mode :  server.express       : express app instance
 *                                          :  server.onconnection  : { action: "function", message: "string" }
 *                        : for client mode :  server.url:  ex: "wss://echo.websocket.org:8080"
 *                                          :  server.onopen        : { action: "function", message: "string" }
 *
 * @param {express app instance} express : nullable for client mode
 */
module.exports.MyWebSocket = function (server, expressInstance) {
    const obj = {};
    const _MODE = !server || !server.url ? 'server' : 'client';
    // const _wss =
    //     _MODE === 'client'
    //         ? new ReconnectingWebSocket(server.url, null, { debug: true, reconnectInterval: 4000 })
    //         : new WebSocket.Server({ noServer: true });
    let _wss = _MODE === 'client' ? new WebSocket(server.url) : new WebSocket.Server({ noServer: true });
    const _onMessageEventRegistry = [];
    const _onConnectionOpenEventRegistry = [];
    let clientModeReconnectTimeout = null;

    /**
     * Define websocket mode: Server or Client
     * @private
     */
    function init() {
        switch (_MODE) {
            case 'server':
                initializeServer();
                break;
            case 'client':
                initializeClient();
                break;
        }
    }

    /**
     * Initializes server mode websocket
     * @private
     */
    function initializeServer() {
        if (!server) server = { onconnection: null };
        // obj.clients = [];
        server.onconnection = !!server.onconnection ? validateHandlerParams(server.onconnection) : validateHandlerParams(null);
        onconnection(server.onconnection);
        if (expressInstance)
            expressInstance.on('upgrade', (request, socket, head) => {
                _wss.handleUpgrade(request, socket, head, (callbackSocket) => {
                    _wss.emit('connection', callbackSocket, request);
                });
            });
    }

    /**
     * On message handler for Server mode
     * When both action and custom is set to null, simple fire respond an echo message
     *      Like so:     { echo: [original_message] }
     *      Warning:     Repeated command registration will overwrite previous
     *
     * If action and custom are both not null, custom will prevail.
     *
     * @private
     * @param
     */
    function onClientMessage(client, message) {
        message = util.getJson(message);
        if (_onMessageEventRegistry.length < 1) sendEchoMessage(message);
        _onMessageEventRegistry.forEach(({ module, command, action }) => {
            if (client.module === module && (message.command === command || command === '//ANY_COMMAND')) action(client, message);
        });
    }

    /**
     * Server mode On Connection event handler registration
     * Sends message if not null and runs actions if not null
     * @private
     * @param {function} action
     * @param {string} message
     */
    function onconnection({ action, message }) {
        _wss.on('connection', (socket, request) => {
            const params = request.url.slice(1, -1).split('/');
            let module, sessionId;
            if (!!params[0]) module = params[0];
            if (!!params[1]) sessionId = params[1];
            if (typeof message === 'string') socket.send(message);
            if (typeof action === 'function') action(socket, request);

            const client = { socket, module, sessionId, params };

            _onConnectionOpenEventRegistry.forEach((handler) => {
                handler(client);
            });

            // On message call handler
            socket.on('message', (callbackMessage) => {
                onClientMessage(client, callbackMessage);
            });
        });
    }

    /**
     * Initializes client mode web socket
     * @private
     */
    function initializeClient() {
        if (!server) server = { onopen: null };
        server.onopen = !!server.onopen ? validateHandlerParams(server.onopen) : validateHandlerParams(null);
        onopen(server.onopen);

        _wss.on('message', (message) => {
            onServerMessage(message);
        });

        _wss.on('error', function (event) {
            util.writeLogText(event);
            _wss.close();
        });

        _wss.on('open', function (event) {
            clearTimeout(clientModeReconnectTimeout);
        });

        _wss.on('close', function (event) {
            util.writeLogText('Try reconnect to core...');
            clientModeReconnectTimeout = setTimeout(reconnectToServer, config.server.coreSocketReconnectInterval);
        });
    }

    /**
     * Reconnect to server when in client mode
     * @private
     */
    function reconnectToServer() {
        _wss = new WebSocket(server.url);
        init();
    }

    /**
     * Client mode On Open event handler registration
     * Sends message if not null and runs actions if not null
     * @private
     * @param {function} action
     * @param {string} message
     */
    function onopen({ action, message }) {
        _wss.on('open', function open() {
            if (typeof message === 'string') _wss.send(message);
            if (typeof action === 'function') action(_wss);
            _onConnectionOpenEventRegistry.forEach((handler) => {
                handler(_wss);
            });
        });
    }

    /**
     * On server message handler for websocket client mode
     * Maps an action for each event
     * @param {object} message
     */
    function onServerMessage(message) {
        message = util.getJson(message);
        if (_onMessageEventRegistry.length < 1) sendEchoMessage(message);
        _onMessageEventRegistry.forEach(({ event, action }) => {
            if (message.event === event && typeof action === 'function') action(_wss, message);
        });
    }

    /**
     * Sends an echo object
     * @param {object} message
     */
    function sendEchoMessage(message) {
        socket.send(JSON.stringify({ echo: !message.badFormat ? message : message.message }));
    }

    /**
     * Validate handler object data
     * @private
     * @param {object} data
     */
    function validateHandlerParams(data = null) {
        if (!data) return { action: null, message: null };
        if (!!data && typeof data.action !== 'function') data.action = null;
        if (!!data && typeof data.message !== 'string') data.message = null;
        return data;
    }

    /**
     * Registers a function to execute on:
     *      connection when in Server mode
     *      open when in client mode
     * @public
     * @param {function} handlerFunction : params:
     *            client on server mode, like:   handlerFunction({ socket, module, sessionId, params })
     *            socket on client mdde, like:   handlerFunction(socket)
     */
    obj.registerOnConnectionOpenHandler = (handlerFunction) => {
        _onConnectionOpenEventRegistry.push(handlerFunction);
    };

    /**
     * On message event registration
     * @public
     *
     * @param {string}   command  : execute action if command matches ex: message = { command: "string", ... }
     * @param {string}   module   : execute action if module matches
     *  	             Example  : wss://echo.websocket.org/:module/:param/:otherParams
     * @param {function} action   : function to execute on new message
     */
    obj.registerOnMessageHandler = (handlerSettings) => {
        _onMessageEventRegistry.push(handlerSettings);
    };

    /**
     * Register a batch of actions
     * @param {string} module  : execute action if module matches
     * @param {*} actions      : function to execute on new message
     */
    obj.commandBatchRegister = (module, actions) => {
        Object.entries(actions).forEach(([command, action]) => {
            obj.registerOnMessageHandler({
                module: module,
                command: command,
                action: action,
            });
        });
    };

    /**
     * Backdoor to use the socket send function
     */
    obj._send = (message) => _wss.send(message);

    // Initialize server and return instance
    init();
    return obj;
};

/**
 * Sends echo responses plus original message object to socket
 * @public
 * @static
 * @param {object} socket
 * @param {string} message
 */
module.exports.echo = {
    ok: (socket, message) => {
        if (!socket) return;
        socket.send(JSON.stringify({ command: 'ok', echo: message }));
    },
    reject: (socket, message) => {
        if (!socket) return;
        socket.send(JSON.stringify({ command: 'rejected', echo: message }));
    },
    unauthorized: (socket, message) => {
        if (!socket) return;
        socket.send(JSON.stringify({ command: 'unauthorized', echo: message }));
    },
    error: (socket, message) => {
        if (!socket) return;
        socket.send(JSON.stringify({ command: 'error', echo: message }));
    },
    redundant: (socket, message) => {
        if (!socket) return;
        socket.send(JSON.stringify({ command: 'redundant', echo: message }));
    },
};

/**
 * Send redundant and echo object to socket
 * @public
 * @static
 * @param {object} socket
 * @param {function} callback : no argument callback function
 */
function closeConnectionMessage(socket, callback) {
    socket.send(JSON.stringify({ command: 'connectionClosed', message: config.server.dictionary.en }));
    callback();
}
module.exports.closeConnectionMessage = closeConnectionMessage;

/**
 * Set destroy timer for this socket
 * @public
 * @static
 * @param {object} socket
 * @param {integer} milliseconds
 * @param {function} action : runs on timeout
 * @param {array} params : parameters for action funtion
 */
function destroySocketOntimeout(socket, milliseconds, action, params = []) {
    const destroyConnection = function () {
        if (typeof action === 'function') action(...params);
        closeConnectionMessage(socket, () => {
            socket.close();
        });
    };
    const timeoutRef = setTimeout(destroyConnection, milliseconds);
    const returnValue = { action, params, socket, timeoutRef, milliseconds, timestamp: Date.now() };
    return returnValue;
}
module.exports.destroySocketOntimeout = destroySocketOntimeout;

/**
 * Renew socket timer
 * @static
 * @param {object} timeout
 */
module.exports.renewDestroySocketTimer = (timeout) => {
    if (!timeout) return null;
    clearTimeout(timeout.timeoutRef);
    return destroySocketOntimeout(timeout.socket, timeout.milliseconds, timeout.action, timeout.params);
};

/**
 * setTimoute reference, returns remaining time in milliseconds
 * @static
 * @param {object} timeoutRef
 */
module.exports.remainingTimeBeforeDestroy = (timeout) => {
    return timeout.timestamp + timeout.milliseconds - Date.now();
};
