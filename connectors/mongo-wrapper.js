/**
 * @module mongo-wrapper
 * Module exports usefull CRUD functions in more cleaned fashion for easier DB access
 *
 * @package connectors
 *
 * @returns following methods:
 * @public testConnection, findOne, findMany, insert, update, delete
 *
 * @author Seal the Deal - Team
 * @version 1.0.1
 * @date 2020/11/28
 * @lastrevision
 *
 */
require('dotenv').config();
const MongoClient = require('mongodb').MongoClient;
const URI = `mongodb+srv://${process.env.ATLAS_USER}:${process.env.ATLAS_KEY}@cluster0.tmfeg.mongodb.net/operational?retryWrites=true&w=majority`;
const connectionOptions = require('../config.json').database.mongoConnectionOptions;
connectionOptions.poolSize = process.env.MONGO_POOLSIZE || connectionOptions.poolSize;

module.exports = {
    /**
     * Tests the connection
     * @param callback: function
     */
    testConnection: (callback) => {
        if (typeof callback !== 'function') return;
        getConnection((error, client) => {
            if (!error) client.close();
            callback(error, !error);
        });
    },

    /**
     * Find one row
     * @param query: {
     *      filter: {object},
     * }
     * @param collection: Name of the collection
     * @param callback: function
     */
    findOne: (query, collection, callback) => {
        if (typeof callback !== 'function') return;
        getConnection((conn_error, client) => {
            if (conn_error) return callback(conn_error, {});
            const db = client.db();
            db.collection(collection).findOne(query.filter, (error, result) => {
                callback(error, !error && !!result ? result : {});
                client.close();
            });
        });
    },

    /**
     * Find many rows
     * @param query: {
     *      filter: {object},
     * }
     * @param collection: Name of the collection
     * @param callback: function
     */
    findMany: (query, collection, callback) => {
        if (typeof callback !== 'function') return;
        getConnection((conn_error, client) => {
            if (conn_error) return callback(conn_error, {});
            const db = client.db();
            db.collection(collection)
                .find(query.filter, query.opts)
                .toArray((error, result) => {
                    callback(error, !error && !!result ? result : {});
                    client.close();
                });
        });
    },

    /**
     * Insert one or more rows
     * @param query: {
     *      data: {object} : values to be inserted
     * }
     * @param collection: Name of the collection
     * @param callback: function
     */
    insert: (data, collection, callback) => {
        if (typeof callback !== 'function') callback = (...arg) => {};
        if (data === undefined) return callback({ error: 'data is undefined', insertedId: null });
        insertOne(data, collection, callback);
    },

    /**
     * Update many rows
     * @public
     * @param query: {
     *      filter: {object}, : find the records
     *      set: {object},    : new values
     *      opts {upsert: boolean, ... }   : example, insert if doenst exist
     * }
     * @param collection: Name of the collection
     * @param callback: function
     */
    update: (query, collection, callback) => {
        if (typeof callback !== 'function') callback = (...arg) => {};
        query.set.updatedTS = new Date();
        getConnection((conn_error, client) => {
            if (conn_error) return callback(conn_error, null);
            const db = client.db();
            db.collection(collection).updateMany(query.filter, { $set: query.set }, query.opts, (error, result) => {
                callback(
                    error,
                    !error
                        ? {
                              modifiedCount: result.modifiedCount,
                              upsertedId: result.upsertedId ? result.upsertedId._id : null,
                              upsertedCount: result.upsertedCount,
                              matchedCount: result.matchedCount,
                          }
                        : null
                );
                client.close();
            });
        });
    },

    /**
     * Delete many rows
     * @public
     * @param query: {
     *      ___deleteAll___: boolean
     *      filter: {object},
     * }
     * @param collection: Name of the collection
     * @param callback: function
     */
    delete: (query, collection, callback) => {
        if (typeof callback !== 'function') callback = (...arg) => {};
        getConnection((conn_error, client) => {
            if (conn_error) return callback(conn_error, { deletedCount: 0 });
            if (query.filter === undefined) {
                query.filter = { ___ABORTED___: true };
                return callback('No filter passed, to delete all use property { ___deleteAll___: true }', null);
            }
            if (Object.keys(query.filter).length < 1) {
                query.filter = { ___ABORTED___: true };
                return callback('No filter passed, to delete all use property { ___deleteAll___: true }', null);
            }
            if (query.___deleteAll___ === true) query.filter = {};
            const db = client.db();
            db.collection(collection).deleteMany(query.filter, (error, result) => {
                callback(error, { deletedCount: !error ? result.deletedCount : null });
                client.close();
            });
        });
    },
};

/**
 * @private
 * Aquires and returns a valid MongoDB connection
 * @param callback: function
 */
function getConnection(callback) {
    if (typeof callback !== 'function') return;
    const client = new MongoClient(URI, connectionOptions);
    client.connect((error) => {
        if (error) return callback(error, null);
        callback(null, client);
    });
}

/**
 * @private
 * Insert one row
 * @param {*} query
 * @param {*} collection
 * @param {*} callback
 */
function insertOne(data, collection, callback) {
    data.creationTS = new Date();
    getConnection((conn_error, client) => {
        if (conn_error) return callback(conn_error, null);
        const db = client.db();
        db.collection(collection).insertOne(data, (error, result) => {
            callback(error, !error && result.insertedCount > 0 ? { insertedId: result.insertedId } : null);
            client.close();
        });
    });
}
