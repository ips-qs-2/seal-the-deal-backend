const axios = require('axios');

const coreApi = axios.create({
    baseURL: 'https://' + process.env.CORE_HTTP_SERVER,
    headers: {
        'Content-Type': 'application/json',
    },
    timeout: 8000,
});

/**
 * allOrganizations
 * @param none
 */
module.exports.getNewCoreToken = async () => {
    try {
        const response = await coreApi.get('/session/get', {
            params: { 'game-id': process.env.CORE_GAME_ID },
            auth: {
                username: process.env.CORE_USERNAME,
                password: process.env.CORE_PASSWORD,
            },
        });
        return response.data;
    } catch (error) {
        return { success: false };
    }
};
