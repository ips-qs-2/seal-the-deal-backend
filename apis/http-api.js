/**
 * @module http-api
 * Module exports a router with all api end-points
 *
 * @package apis
 *
 * @returns following methods:
 * @public testConnection, findOne, findMany, insert, update, delete
 *
 * @author Seal the Deal - Team
 * @version 1.0.1
 * @date 2020/11/28
 * @lastrevision
 *
 */
require('dotenv').config();
const express = require('express');
const router = express.Router();
const gameController = require('../controllers/game-controller');
const playerController = require('../controllers/player-controller');

// Api online
router.get('/ping', (req, res) => {
    res.status(200).send('Pong! Pang!');
});

// Player
router.get('/players', playerController.allPlayers);
router.get('/players/:id', playerController.one);

//router.get('/rank', playerController.getRanking);  //Edna, era setRanking ou getRanking. Comentei porque estava a dar erro aqui

// Game
router.get('/games', gameController.allGames);
router.get('/players/:email/games', gameController.getById);
//router.get('/game/:sessionId', gameController.classificacao);

module.exports = router;
