/**
 * @module util
 * Module containing multiple utility functions
 *
 * @package root
 *
 * @returns following methods:
 * @public exposeRequest
 *
 * @author Seal the Deal
 * @version 1.0.1
 * @date 2020/11/27
 * @lastrevision
 *
 */
// Import modules
const config = require('./config.json');
const fs = require('fs');

/**
 * Exposes HTTP request method, baseUrl, paramenters, query and body
 *
 * @param {object} req
 * @param {object} res
 * @param {function} next
 */
module.exports.exposeRequest = (req, res, next) => {
    const title = '::: NEW REQUEST';
    console.log('_________________________________________________________');
    console.log(`\x1b[36m${title}\x1b[0m`);
    console.log("'\x1b[33m%s\x1b[0m',", req.method, ': ', req.baseUrl);
    console.log('::: REQ.PARAMS: \n', req.params);
    console.log('::: REQ.QUERY: \n', req.query);
    console.log('::: REQ.BODY: \n', req.body);
    console.log('_________________________________________________________');
    next();
};

/**
 * Returns JSON parsed string or null
 * @param {string} message
 */
module.exports.getJson = (str) => {
    let message;
    try {
        message = JSON.parse(str);
    } catch (e) {
        message = { badFormat: true, message: str };
    }
    return message;
};

module.exports.uid = () => {
    const ObjectID = require('mongodb').ObjectID;
    return new ObjectID();
};

/**
 * Queue data struture
 */
function Queue() {
    this.elements = [];

    Queue.prototype.enqueue = function (e) {
        this.elements.push(e);
    };

    Queue.prototype.dequeue = function () {
        return this.elements.shift();
    };

    Queue.prototype.isEmpty = function () {
        return this.elements.length == 0;
    };

    Queue.prototype.peek = function () {
        return !this.isEmpty() ? this.elements[0] : undefined;
    };

    Queue.prototype.hasElement = function (e, compareFn) {
        const that = this;
        let doesMatch = false;
        const iterator = function (i) {
            if (i < that.length()) {
                if (compareFn(that.elements[i], e)) doesMatch = true;
                else iterator(i + 1);
            }
        };
        iterator(0);
        return doesMatch;
    };

    Queue.prototype.length = function () {
        return this.elements.length;
    };
}
module.exports.Queue = Queue;

/**
 * Recursive iterator to process order total cost
 * @param {object[]} order
 * @param {function} callback
 */
function getTotalOrderValue(order) {
    let total = 0;
    const pricesArray = [];
    config.store.catalog.forEach((power) => pricesArray.push(parseFloat(power.price)));
    const processOrder = function (i) {
        if (i < order.length) {
            total += pricesArray[order[i].id - 1] * order[i].amount;
            processOrder(i + 1);
        }
    };
    processOrder(0);
    return total;
}
module.exports.getTotalOrderValue = getTotalOrderValue;

/**
 * Recursive iterator to process order powerup stock
 * @param {object[]} order
 * @param {function} callback
 */
function getUpdatedPowerupStock(originalPowerups, order) {
    const catalog = config.store.catalog;
    order.forEach((powerup) => (originalPowerups[catalog[powerup.id - 1].name] += powerup.amount));
    return { ...originalPowerups };
}
module.exports.getUpdatedPowerupStock = getUpdatedPowerupStock;

/**
 * Writes to log file
 * @param {string} text
 */
function writeLogText(text) {
    try {
        if (!fs.existsSync(config.server.logFile)) {
            fs.writeFile(config.server.logFile, '', () => {});
        }
    } catch (err) {
        console.error(err);
    }
    try {
        const date = new Date().toJSON().replace(new RegExp(':', 'g'), '.');
        text = '\n *** ' + date + '  ::  ' + text;
        getLog((err1, result1) => {
            if (err1) return;
            if (result1.length > 3000) fs.writeFile(config.server.logFile, text, () => {});
            else fs.appendFile(config.server.logFile, text, () => {});
        });
    } catch {}
}
module.exports.writeLogText = writeLogText;

/**
 * Read from log file
 */
function getLog(callback) {
    fs.readFile(config.server.logFile, 'utf8', function (err, data) {
        if (err) {
            return callback(err, null);
        }
        return callback(null, data);
    });
}
module.exports.getLog = getLog;

/**
 * Função exemplo
 */
module.exports.soma = (a, b) => {
    return a + b;
};
