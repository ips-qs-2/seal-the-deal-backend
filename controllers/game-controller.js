/**
 * @module player-controller
 * Manages networking between models and game-manager actions on the game business side
 *
 * @package controllers
 *
 * @returns following methods:
 * @public allGames
 *
 * @author Seal the Deal - Team
 * @version 1.0.1
 * @date 2020/11/28
 * @lastrevision
 *
 */
const config = require('../config.json');
const Game = require('../models/game');
const wsWrapper = require('../connectors/ws-wrapper');

////////////////////////////////////////////////////////////////////////
// WebSoctes controls

/**
 * Forwards WsWrapper Response Codes
 */
module.exports.echo = wsWrapper.echo;

/**
 * Creates / Obtains new user
 * @param {object} data : {id, name, birthday, mail, avatar}
 * @param {function} callback
 */
module.exports.newGame = (data, callback) => {
    const game = new Game(data.playerOne, data.playerTwo, Date.now());
    game.save((err, success) => {
        if (err) return callback(err, null);
        return callback(null, game);
    });
};

/**
 * Send to both players of a game when match has started
 */
module.exports.startedMatch = (clientOne, clientTwo, sharks) => {
    if (!clientOne || !clientTwo) return;
    clientOne.socket.send(
        JSON.stringify({
            command: 'startedMatch',
            data: {
                turnToPlay: clientOne.player.turnToPlay,
                lifetime: config.game[clientOne.player.mode].lifetime,
                lifes: clientOne.player.lifes,
                opponent: {
                    name: clientTwo.player.name,
                    points: clientTwo.player.points,
                    mode: clientTwo.player.mode,
                    lifes: clientTwo.player.lifes,
                    avatar: clientTwo.player.avatar,
                    lifetime: config.game[clientTwo.player.mode].lifetime,
                },
                sharks: sharks,
            },
        })
    );
    clientTwo.socket.send(
        JSON.stringify({
            command: 'startedMatch',
            data: {
                turnToPlay: clientTwo.player.turnToPlay,
                lifetime: config.game[clientTwo.player.mode].lifetime,
                lifes: clientTwo.player.lifes,
                opponent: {
                    name: clientOne.player.name,
                    points: clientOne.player.points,
                    mode: clientOne.player.mode,
                    lifes: clientOne.player.lifes,
                    avatar: clientOne.player.avatar,
                    lifetime: config.game[clientOne.player.mode].lifetime,
                },
                sharks: sharks,
            },
        })
    );
};

/**
 * Send opponent play
 * @param {object} client
 * @param {object} data
 */
module.exports.opponentPlay = (client, data) => {
    client.socket.send(
        JSON.stringify({
            command: 'opponentPlay',
            data: {
                opponent: data.me,
                sharks: data.sharks,
            },
        })
    );
};

/**
 * Sends message to clients that were bitten
 * @param {*} client
 * @param {*} data
 */
module.exports.sendBittenNotification = (sharksBytes, socket) => {
    if (!sharksBytes) return;
    socket.send(
        JSON.stringify({
            command: 'sharksBytes',
            data: {
                me: sharksBytes.me,
                opponent: sharksBytes.opponent,
            },
        })
    );
};

/**
 *
 * @param {Force user to terminate turn by timeout} client
 */
module.exports.endUserTurnByTimeout = (client) => {
    client.socket.send(
        JSON.stringify({
            command: 'turnTimeout',
        })
    );
};

/**
 * Send game state to client.
 * @param {object} client
 * @param {object} data
 */
module.exports.sendGameState = (client, data) => {
    client.socket.send(
        JSON.stringify({
            command: 'setGameState',
            data: data,
        })
    );
};

/**
 *
 * @param {object} client1
 * @param {object} client2
 * @param {Game} game
 */
module.exports.gameOver = (client1, client2, game) => {
    game.save((err, result) => {
        if (err) return;
    });
    client1.socket.send(
        JSON.stringify({
            command: 'gameover',
            data: {
                result: game.result[0],
            },
        })
    );
    client2.socket.send(
        JSON.stringify({
            command: 'gameover',
            data: {
                result: game.result[1],
            },
        })
    );
    classificacao(client1.socket, client1.sessionId)
    classificacao(client2.socket, client2.sessionId)
};

/**
 * Send classification to client
 * @param {object} socket 
 * @param {string} sessionId 
 */
function classificacao(socket, sessionId) {
  let gamesR=[];
    Game.many({ filter: { $or: [{ "playerOne.sessionId": sessionId} ,{ "playerTwo.sessionId": sessionId}] }, opts: { limit: 10, sort: { $natural: -1 } } }, (error, result) => {
    result.forEach(player => {
      if(player.result){
        if(sessionId===player.playerOne.sessionId ) 
        {           gamesR.push({me:player.playerOne, opponent: player.playerTwo, result:player.result[0],startedTS:player.startedTS, endedTS:player.endedTS})
        }
      else {gamesR.push({me:player.playerTwo, opponent: player.playerOne, result:player.result[1],startedTS:player.startedTS, endedTS:player.endedTS}) }
    }
    });    
    socket.send(
        JSON.stringify({
            command: 'setClassify',
            data: gamesR,
        })
    );
});
};
module.exports.classificacao = classificacao


////////////////////////////////////////////////////////////////////////
// HTTP controls

module.exports.allGames = (req, res) => {
    Game.many({opts: { limit: 20, sort: { $natural: -1 } } }, (error, result) => {
        if (error) return res.sendStatus(500);
        if (result.length < 1) return res.sendStatus(404);
        return res.json(result);
    });
};

module.exports.getById = (req, res) => {
    Game.many({ filter: { "playerOne.email": req.params.email }, opts: { limit: 20, sort: { $natural: -1 } } }, (error, result) => {
        if (error) return res.sendStatus(500);
        if (result.length < 1) return res.sendStatus(404);
        return res.json(result);
    });
};

