/**
 * @module player-controller
 * Manages networking between models and game-manager actions on the player business side
 *
 * @package controllers
 *
 * @returns following methods:
 * @public allPlayers
 *
 * @author Seal the Deal - Team
 * @version 1.0.1
 * @date 2020/11/28
 * @lastrevision
 *
 */

// Import modules
const config = require('../config.json');
const util = require('../util');
const Player = require('../models/player');
const wsWrapper = require('../connectors/ws-wrapper');

///////////////////////////////////////////////////////////////////////////////////////////
// WebSockets controller

/**
 * Forwards WsWrapper Response Codes
 */
module.exports.echo = wsWrapper.echo;

/**
 * Creates / Obtains new user
 * @param {object} data : {id, name, birthday, mail, avatar}
 * @param {function} callback
 */
module.exports.newPlayer = (sessionId, data, callback) => {
    if (typeof data.id === 'undefinded') return callback('Missing the player id...', null);
    // if (typeof req.player === "undefined") return callback("Missing the player data...", null);
    const newplayer = new Player(data.id, data.name || null, data.birth_date || null, data.mail || null, data.avatar || null);
    newplayer.save((errorPlayerSave, result) => {
        if (errorPlayerSave) return callback(errorPlayerSave, null);
        Player.one(newplayer.id, (errorOnePlayer, player) => {
            if (errorOnePlayer) return callback(errorOnePlayer, null);
            return callback(null, player);
        });
    });
};

/**
 * Send a setPlayer command to the client with the user data
 * @param {Socket} socket
 * @param {Player} player
 */
module.exports.setPlayer = (socket, player) => {
    socket.send(
        JSON.stringify({
            command: 'setPlayer',
            data: { ...player, points: player.points, credits: player.credits, powerups: player.powerups },
        })
    );
};

/**
 * Update points
 * @param {object} req : {id, points}
 * @param {function} callback : (error, result)
 */
module.exports.savePoints = (req, callback) => {
    if (typeof req.id === 'undefinded') return callback('Missing the player id...', null); // TODO converter erros em objetos de erros
    if (typeof req.points === 'undefined') return callback('Missing the points...', null); // TODO converter erros em objetos de erros
    Player.one(req.params.id, (errorPlayerOne, player) => {
        if (errorPlayerOne) return callback(errorPlayerOne, null);
        if (!player.id) return callback(`Player id: ${player.id} not found...`);
        player.savePoints(req.body.points, (errorSavePoints, result) => {
            if (errorSavePoints) return callback(errorSavePoints, null);
            if (result) return callback(null, result);
        });
    });
};

/**
 * Update credits
 * @param {object} req : {id, credits}
 * @param {function} callback : (error, result)
 */
module.exports.saveCredits = (req, callback) => {
    if (typeof req.id === 'undefinded') return callback('Missing the player id...', null);
    if (typeof req.credits === 'undefined') return callback('Missing the credits...', null);
    Player.one(req.params.id, (errorPlayerOne, player) => {
        if (errorPlayerOne) return callback(errorPlayerOne, null);
        if (!player.id) return callback(`Player id: ${player.id} not found...`);
        player.saveCredits(req.body.credits, (errorSaveCredits, result) => {
            if (errorSaveCredits) return callback(errorSaveCredits, null);
            if (result) return callback(null, result);
        });
    });
};

/**
 * Send store prices to client
 * @param {Socket} socket
 * @param {Player} player
 */
module.exports.setStore = (socket, player) => {
    socket.send(
        JSON.stringify({
            command: 'setStore',
            data: config.store.catalog,
        })
    );
    this.setPlayer(socket, player);
};

/**
 * Buy powerups with credits
 * @param {Socket} socket
 * @param {Player} player
 */
module.exports.buyPowerups = (socket, player, request) => {
    // Process order total cost
    const orderTotal = util.getTotalOrderValue(request.data.powerups);
    // Check player
    Player.one(player.id, (err, updatedPlayer) => {
        if (err) return wsWrapper.echo.unauthorized(socket, request);
        // Backup player
        const playerBackup = updatedPlayer;
        // Previous powerups
        const powerups = updatedPlayer.powerups ? updatedPlayer.powerups : { seal: 0, speed: 0, scent: 0, radar: 0 };
        const updatedPowerupState = util.getUpdatedPowerupStock(powerups, request.data.powerups);
        if (updatedPlayer.credits < orderTotal) return wsWrapper.echo.reject(socket, request);
        player.credits -= orderTotal;
        updatedPlayer.saveCredits(player.credits, (errorCredits, resultAfterDebit) => {
            if (errorCredits) return wsWrapper.echo.error(socket, request);
            player.savePowerups(updatedPowerupState, (errorPowerups, resultPowerupsPlayer) => {
                if (errorPowerups) {
                    playerBackup.saveCredits(playerBackup.credits);
                    playerBackup.save();
                    return wsWrapper.echo.error(socket, request);
                }
                this.setPlayer(socket, player);
                return wsWrapper.echo.ok(socket, request);
            });
        });
        this.setPlayer(socket, player);
    });
};

/**
 * Update powerup state
 * @param {Socket} socket
 * @param {Player} player
 */
module.exports.updatePowerupState = (socket, player, request) => {
    const powerups = player.powerups;
    if (powerups[request.powerup] < 1) return;
    powerups[request.powerup] -= 1;
    player.savePowerups(powerups, (errorPowerups, resultPowerupsPlayer) => {
        if (errorPowerups) {
            playerBackup.saveCredits(playerBackup.credits);
            playerBackup.save();
            return wsWrapper.echo.error(socket, request);
        }
        this.setPlayer(socket, player);
        return wsWrapper.echo.ok(socket, request);
    });
};
// ranking
function Ranking(socket, sessionId) {
    Player.many({ opts: { limit: 10, sort: { points: -1 } } }, (error, players) => {
        if (error) return wsWrapper.echo.error(socket, request);
        socket.send(
            JSON.stringify({
                command: 'setRanking',
                data: players,
            })
        );
    });
}
module.exports.Ranking = Ranking;
///////////////////////////////////////////////////////////////////////////////
// HTTP Controllers

/**
 * Returns all Players in database
 * @param {object} req
 * @param {object} res
 */
module.exports.allPlayers = (req, res) => {
    Player.many({}, (error, players) => {
        if (error) return res.sendStatus(500);
        if (players.length < 1) return res.sendStatus(404);
        return res.json(players);
    });
};

/**
 * Returns one player by id
 * @param {object} req
 * @param {object} res
 */
module.exports.one = (req, res) => {
    if (typeof req.params.id === 'undefinded') return res.sendStatus(400);
    Player.one(parseInt(req.params.id), (error, player) => {
        if (error) return res.sendStatus(500);
        if (!player.id) return res.sendStatus(404);
        return res.json({ id: player.id, ...player, points: player.points, credits: player.credits });
    });
};
