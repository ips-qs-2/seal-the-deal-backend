/**
 * @module games-manager
 * Handles games logic and validations
 *
 * @package managers
 *
 * @returns following methods:
 *
 * @author Seal the Deal - Team
 * @version 1.0.1
 * @date 2020/11/28
 * @lastrevision
 *
 */

// Import modules
const util = require('../util');
const config = require('../config.json');
const gameController = require('../controllers/game-controller');
const playerController = require('../controllers/player-controller');
const { response } = require('express');

module.exports.Games = function (clients) {
    // Module return object
    const obj = {};

    // Module data structures
    const playersQueue = new util.Queue();
    const games = {};

    // Global constants
    const ONE_LIFE = 1;
    const FLAG_WON = 1;
    const FLAG_LOST = -1;
    const FLAG_DRAW = 0;
    const globalVariable = {};

    /**
     * Register a global variable
     * @param {function} action
     * @param {params} params
     */
    obj.addGlobalVariable = (name, value) => {
        globalVariable[name] = value;
    };

    /**
     * Unregister a global variable
     */
    obj.removeGlobalVariable = (varName) => {
        delete globalVariable[varName];
    };

    /**
     * This function is for development only.
     * Delete on actual production situation
     * @public
     */
    obj.peek = () => {
        const activeGames = {};
        const iterator = function (i) {
            const gamesKeys = Object.keys(games);
            if (i < gamesKeys.length) {
                activeGames[gamesKeys[i]] = { player1: games[gamesKeys[i]].playerOne, player2: games[gamesKeys[i]].playerTwo };
                iterator(i + 1);
            }
        };
        iterator(0);
        return activeGames;
    };

    /**
     * Register client response handler
     * @public
     * @param {object} client
     * @param {object} message
     */
    obj.handlePlayerResponse = (client, message) => {
        const player = clients.get(client.sessionId).player;
        const game = getPlayersGame(player);
        switch (message.echo.command) {
            case 'startedMatch':
                if (player.turnToPlay) {
                    game.turns = [];
                    game.timeout = setTimeout(terminateTurnByTimeout, config.game[player.mode].lifetime, client);
                }
                break;
            case 'opponentPlay':
                game.timeout = setTimeout(terminateTurnByTimeout, config.game[player.mode].lifetime, client);
                break;
            case 'gameOver':
                game.timeout = null;
                break;
        }
    };

    /**
     * Register event handler to deal with requests to start match
     * @public
     * @param {object} client
     * @param {object} message
     */
    obj.startMatch = (client, message) => {
        // Process player request
        if (message.data.mode === '') return gameController.echo.error(client.socket, message);
        if (isPlayerInQueue(clients.getPlayer(client.sessionId))) return gameController.echo.redundant(client.socket, message);
        else {
            if (!message.data.mode) return;
            // Register new player request
            const newPlayer = clients.getPlayer(client.sessionId);
            newPlayer.mode = message.data.mode;
            newPlayer.sessionId = client.sessionId;
            newPlayer.lifes = config.game[message.data.mode].lifes;
            // If there are players in the queue, make a match and create a game instance
            if (playersQueue.length() > 0) {
                const waitingPlayer = playersQueue.dequeue();
                createNewGame(waitingPlayer, newPlayer);
            } // Otherwise, enqueue the new player
            else playersQueue.enqueue(newPlayer);
            // Feedback message to player
            gameController.echo.ok(client.socket, message);
        }
    };

    /**
     * Register event handler to deal with the a player move
     * @public
     * @param {object} client
     * @param {object} message
     */
    obj.play = (client, message) => {
        updatePlayers(client, message);
        updateGameState(client, message);

        // Check if game has ended before execute move
        const game = getPlayersGame(clients.getPlayer(client.sessionId));
        if (game.result == null) executeMove(client, message);
    };

    /**
     * Return a game from the player
     * @public
     * @param {Player} player
     */
    obj.getGameByPlayer = (player) => {
        return getPlayersGame(player);
    };
 /**
     * Provide store items and prices
     * @param {object} client
     * @param {object} message
     */
    obj.getClassify = (client, message) => {
        if (!clients.hasPlayer(client.sessionId)) {
            playerController.echo.unauthorized(client.socket, message);
        } else {
            gameController.classificacao(client.socket, client.sessionId);
            gameController.echo.ok(client.socket, message);
        }
    };

    /**
     * Auxiliary function to execute players move
     * @private
     * @param {object} client
     * @param {object} message
     */
    function executeMove(client, message) {
        const player = clients.getPlayer(client.sessionId);
        const opponent = getOpponentFrom(player);
        const game = getPlayersGame(player);
        if (!player.turnToPlay || !game) gameController.echo.unauthorized(client.socket, message);
        else {
            clearTimeout(game.timeout);
            player.turnToPlay = false;
            opponent.turnToPlay = true;
            game.turns.push({ player: player.id, move: message.data });
            gameController.opponentPlay(clients.get(opponent.sessionId), message.data);
            gameController.echo.ok(client.socket, message);
        }
    }

    /**
     * Checks if last move ended the game
     * @private
     * @param {object} client
     * @param {object} message
     */
    function updatePlayers(client, message) {
        const player = clients.getPlayer(client.sessionId);
        player.xPos = message.data.me.xPos;
        player.yPos = message.data.me.yPos;
        const turns = getPlayersGame(player).turns;
        if (turns.length < 1) return;
        const opponent = getOpponentFrom(player);
        const sharks = message.data.sharks;
        const playerBytes = [];
        const opponentBytes = [];
        // Iterate sharks to process killings
        const iterator = function (i) {
            if (sharks.length > i) {
                if (opponent.lifes > 0 && opponent.xPos == sharks[i].xPos && opponent.yPos == sharks[i].yPos) {
                    opponent.lifes--;
                    opponentBytes.push(sharks[i]);
                }
                if (player.lifes > 0 && player.xPos == sharks[i].xPos && player.yPos == sharks[i].yPos) {
                    player.lifes--;
                    playerBytes.push(sharks[i]);
                }
                iterator(i + 1);
            }
            opponent.sharksBytes = { me: opponentBytes, opponent: playerBytes };
            player.sharksBytes = { me: playerBytes, opponent: opponentBytes };
            return;
        };
        iterator(0);
    }

    /**
     * Update game state. In terms of lifes
     * @private
     * @param {object} playerClient
     * @param {object} message
     */
    function updateGameState(playerClient, message) {
        const player = clients.getPlayer(playerClient.sessionId);
        const opponent = getOpponentFrom(player);
        const game = getPlayersGame(player);
        // Send state to current player
        gameController.sendGameState(playerClient, buildPlayerStateObject(player, opponent, message.data.sharks));
        // Send state to previous player
        gameController.sendGameState(
            clients.get(opponent.sessionId),
            buildPlayerStateObject(opponent, player, message.data.sharks)
        );
        // Check if games is over
        gameController.sendBittenNotification(player.sharksBytes, clients.getSocket(player.sessionId));
        gameController.sendBittenNotification(opponent.sharksBytes, clients.getSocket(opponent.sessionId));
        if (player.lifes < ONE_LIFE || opponent.lifes < ONE_LIFE) gameOver(game);
    }

    /**
     * Deals with gameover
     * @param {Game} game
     */
    function gameOver(game) {
        // Disable turn timeout
        game.timeout = null;
        // End game timestamp
        game.endedTS = Date.now();
        // Get player instances
        const p1 = game.playerOne;
        const p2 = game.playerTwo;
        const client1 = clients.get(p1.sessionId);
        const client2 = clients.get(p2.sessionId);
        // Add up time spent in games
        client1.timeplayed += game.endTS - game.startedTS;
        client2.timeplayed += game.endTS - game.startedTS;

        p1.turnToPlay = false;
        p2.turnToPlay = false;

        // Check result
        if (p1.lifes < ONE_LIFE && p2.lifes < ONE_LIFE) {
            game.result = [FLAG_DRAW, FLAG_DRAW];
        } else if (p1.lifes < ONE_LIFE) {
            game.result = [FLAG_LOST, FLAG_WON];
        } else if (p2.lifes < ONE_LIFE) {
            game.result = [FLAG_WON, FLAG_LOST];
        }
        gameController.gameOver(client1, client2, game);
        computeRewards(game);
        reportEndGame(game);
    }

    /**
     * Generates report for ended game
     * @param {Game} game
     */
    function reportEndGame(game) {
        const duration = game.endedTS - game.startedTS;
        const resultMapper = { '-1': 'lose', 0: 'draw', 1: 'win' };

        // Message to core template
        const message = {
            event: 'match-end',
            'request-id': globalVariable['REQUEST_ID'],
            data: {
                'game-id': globalVariable['GAME_ID'],
                'match-result': {
                    user_id: null,
                    mode: 'multi-player',
                    difficulty: null,
                    result: null,
                    duration: duration,
                },
            },
        };

        // Send player one result
        message.data['match-result'].user_id = game.playerOne.id;
        message.data['match-result'].difficulty = game.playerOne.mode;
        message.data['match-result'].result = resultMapper[game.result[0]];
        globalVariable['CORE_SOCKET']._send(JSON.stringify(message));
        // Send player two result
        message.data['match-result'].user_id = game.playerTwo.id;
        message.data['match-result'].difficulty = game.playerTwo.mode;
        message.data['match-result'].result = resultMapper[game.result[1]];
        globalVariable['CORE_SOCKET']._send(JSON.stringify(message));
    }

    /**
     * This function credits the player account with
     * a percentage of the points earn credits
     * @private
     * @param {object} game
     */
    function computeRewards(game) {
        // Constants
        const POINT_TO_CREDIT_RATE = config.creditSystem.pointsToCreditRate;
        // Get instances
        const p1 = game.playerOne;
        const p2 = game.playerTwo;
        // Players rewards
        const p1Reward = config.game[p1.mode].reward[game.result[0]];
        const p2Reward = config.game[p2.mode].reward[game.result[1]];

        // Update instance values
        p1.points = p1Reward + p1.points < 0 ? 0 : p1Reward + p1.points;
        p2.points = p2Reward + p2.points < 0 ? 0 : p2Reward + p2.points;
        if (p1Reward > 0) p1.credits += p1Reward * POINT_TO_CREDIT_RATE;
        if (p2Reward > 0) p2.credits += p2Reward * POINT_TO_CREDIT_RATE;

        // DEAL P1
        p1.savePoints(p1.points, (err1, result1) => {
            if (err1) return;
            p1.saveCredits(p1.credits, (err2, result2) => {
                if (err2) return;
                playerController.setPlayer(clients.getSocket(p1.sessionId), p1);
            });
        });
        // DEAL P2
        p2.savePoints(p2.points, (err1, result1) => {
            if (err1) return;
            p2.saveCredits(p2.credits, (err2, result2) => {
                if (err2) return;
                playerController.setPlayer(clients.getSocket(p2.sessionId), p2);
            });
        });
    }

    /**
     * Builds state object to send to client
     * @param {player} player1
     * @param {player} player2
     */
    function buildPlayerStateObject(p1, p2, sharks) {
        const me = { xPos: p1.xPos, yPos: p1.yPos, lifes: p1.lifes, lifetime: p1.lifetime, turnToPlay: p1.turnToPlay };
        const opponent = { xPos: p2.xPos, yPos: p2.yPos, lifes: p2.lifes, lifetime: p2.lifetime, turnToPlay: p2.turnToPlay };
        return { me, opponent, sharks };
    }

    /**
     * Auxiliary function to terminate user turn by timeout
     * @private
     * @param {object} client
     */
    function terminateTurnByTimeout(client) {
        gameController.endUserTurnByTimeout(client);
    }

    /**
     * Auxiliary function to request the creation of new game
     * @private
     * @param {object} playerOne
     * @param {object} playerOne
     */
    function createNewGame(playerOne, playerTwo) {
        // Configure game turns
        playerOne.turnToPlay = true;
        playerTwo.turnToPlay = false;
        // Register new game to DB and send clients
        removeGamesFromPlayers(playerOne, playerTwo, () => {
            gameController.newGame({ playerOne, playerTwo }, (err1, game) => {
                if (err1) return err;
                createSharks((err2, sharks) => {
                    if (err2) return;
                    game.sharks = sharks;
                    games[game._id] = game;
                    gameController.startedMatch(clients.get(playerOne.sessionId), clients.get(playerTwo.sessionId), sharks);
                });
            });
        });
    }

    /**
     * Create initial sharks array
     * @returns {array}
     */
    function createSharks(callback) {
        const sharks = [
            { xPos: 4, yPos: 4 },
            { xPos: 8, yPos: 8 },
            { xPos: 4, yPos: 13 },
        ];
        return callback(null, sharks);
    }

    /**
     * Get opponent from player in param
     * @private
     * @param {object} player
     */
    function getOpponentFrom(player) {
        const game = getPlayersGame(player);
        if (game.playerOne.id === player.id) return game.playerTwo;
        if (game.playerTwo.id === player.id) return game.playerOne;
        else return null;
    }

    /**
     * Get game where player is playing
     * @private
     * @param {object} player
     */
    function getPlayersGame(player) {
        let game = null;
        const iterator = function (i) {
            const gamesKeys = Object.keys(games);
            if (i < gamesKeys.length) {
                if (!!games[gamesKeys[i]])
                    if (games[gamesKeys[i]].playerOne.id === player.id || games[gamesKeys[i]].playerTwo.id === player.id)
                        game = games[gamesKeys[i]];
                    else iterator(i + 1);
            }
        };
        iterator(0);
        return game;
    }

    /**
     * Remove games from these given players
     * @param {Player} p1
     * @param {Player} p2
     * @param {function} callback
     */
    function removeGamesFromPlayers(p1, p2, callback) {
        const game1 = getPlayersGame(p1);
        const game2 = getPlayersGame(p2);
        if (!!game1) delete games[game1._id];
        if (!!game2) delete games[game2._id];
        callback();
    }

    /**
     * Checks if player is already enqueued
     * @private
     * @param {object} player
     */
    function isPlayerInQueue(player) {
        return playersQueue.hasElement(
            player,
            // Compare funtion
            (p1, p2) => p1.id === p2.id
        );
    }
    return obj;
};
