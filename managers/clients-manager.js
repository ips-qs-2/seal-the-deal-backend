/**
 * @module games-manager
 * Handles games logic and validations
 *
 * @package managers
 *
 * @returns following methods:
 *
 * @author Seal the Deal - Team
 * @version 1.0.1
 * @date 2020/11/28
 * @lastrevision
 *
 */

// Import modules
const wsWrapper = require('../connectors/ws-wrapper');
const playerController = require('../controllers/player-controller');

module.exports.Clients = function () {
    // Module return object
    const obj = {};

    // Module data structures
    const clients = {};

    /**
     * This function is for development only.
     * Delete on actual production situation
     * @public
     */
    obj.peek = () => {
        const activeSessionIds = [];
        const keys = Object.keys(clients);
        const iterator = function (i) {
            if (i < keys.length) {
                if (!!clients[keys[i]].socket) {
                    const remainingTime = clients[keys[i]].timeoutAction
                        ? wsWrapper.remainingTimeBeforeDestroy(clients[keys[i]].timeoutAction)
                        : null;
                    activeSessionIds.push({
                        name: clients[keys[i]].player.name,
                        sessionId: keys[i],
                        remainingTime: remainingTime,
                    });
                }
                iterator(i + 1);
            }
        };
        if (keys.length > 0) iterator(0);
        return activeSessionIds;
    };

    /**
     * Returns the client with given id
     * @param {string} sessionId
     */
    obj.get = (sessionId) => {
        return clients[sessionId];
    };

    /**
     * Returns registered client keys
     */
    obj.getRegisteredClients = () => {
        return Object.getOwnPropertyNames(clients);
    };

    /**
     * Returns clients socket
     * @param {string} sessionId
     */
    obj.getSocket = (sessionId) => {
        return clients[sessionId].socket;
    };

    /**
     * Sets socket for client with sessionId
     * @param {*} sessionId
     * @param {*} socket
     */
    obj.setSocket = (sessionId, socket) => {
        clients[sessionId].socket = socket;
    };

    /**
     * Returns the timeout object
     * @param {string} sessionId
     */
    obj.getTimeoutAction = (sessionId) => {
        return clients[sessionId].timeoutAction;
    };

    /**
     * Sets the timeout function for the client
     * @param {string} sessionId
     * @param {function} action :callback action to run on timeout
     */
    obj.setTimeoutAction = (sessionId, action) => {
        clients[sessionId].timeoutAction = action;
    };

    /**
     * Checks if client already is registered
     * @param {string} sessionId
     */
    obj.exists = (sessionId) => {
        return Object.keys(clients).includes(sessionId);
    };

    /**
     * Returns player instance
     * @param {string} sessionId
     */
    obj.getPlayer = (sessionId) => {
        if (!clients[sessionId]) return playerController.echo.unauthorized();
        if (typeof clients[sessionId] === 'undefined') return playerController.echo.unauthorized();
        return clients[sessionId].player;
    };

    /**
     * Checks if client has a player instance registered
     * @param {string} sessionId
     */
    obj.hasPlayer = (sessionId) => {
        return clients.hasOwnProperty(sessionId) && clients[sessionId].hasOwnProperty('player');
    };

    /**
     * Destroys client associated to instance
     * @param {string} sessionId
     */
    obj.destroy = (sessionId) => {
        if (!clients[sessionId].isImmortal) delete clients[sessionId];
        else if (!!clients[sessionId].socket) {
            clients[sessionId].socket.close();
            clients[sessionId] = null;
        }
    };

    /**
     * Gets clients player instance
     * @param {*} client
     * @param {*} message
     */
    obj.sendPlayer = (client, message) => {
        if (!clients.hasOwnProperty(client.sessionId) || !clients[client.sessionId].hasOwnProperty('player')) {
            playerController.echo.unauthorized(client.socket, message);
        } else {
            playerController.setPlayer(client.socket, clients[client.sessionId].player);
            playerController.echo.ok(client.socket, message);
        }
    };

    /**
     * Validates new request for creating user
     * @param {object} user : Player info data { id, name, birthday, email, avatar }
     * @param {string} sessionId : identifing sesions
     * @param {function} callback
     */
    obj.validateSessionId = (user, sessionId, callback, dontKill = false) => {
        // Resets user object if session already active
        clients[sessionId] = null;
        playerController.newPlayer(sessionId, user, (error, player) => {
            if (error && typeof callback === 'function') return callback(error, null);
            createClient(sessionId, player, dontKill);
            if (typeof callback === 'function') return callback(null, player);
        });
    };

    /**
     * Provide store items and prices
     * @param {object} client
     * @param {object} message
     */
    obj.getRanking = (client, message) => {
        if (!clients.hasOwnProperty(client.sessionId) || !clients[client.sessionId].hasOwnProperty('player')) {
            playerController.echo.unauthorized(client.socket, message);
        } else {
            playerController.Ranking(client.socket, clients[client.sessionId].player);
            playerController.echo.ok(client.socket, message);
        }
    };

    /**
     * Creates a new client instance
     * @param {string} sessionId
     * @param {object} player
     */
    function createClient(sessionId, player, dontKill) {
        clients[sessionId] = { player, timeplayed: 0 };
        clients[sessionId].isImmortal = dontKill;
    }

    return obj;
};
