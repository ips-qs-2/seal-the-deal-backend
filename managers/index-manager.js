/**
 * @module game
 * Makes the game logic
 *
 * @package controllers
 *
 * @returns following methods:
 * @public allPlayers
 *
 * @author Seal the Deal - Team
 * @version 1.0.1
 * @date 2020/11/28
 * @lastrevision
 *
 */

// Import modules
const util = require('../util');
const config = require('../config.json');
const wsWrapper = require('../connectors/ws-wrapper');
const playerController = require('../controllers/player-controller');
const clientsManager = require('./clients-manager');
const gamesManager = require('./games-manager');
const powerupsManager = require('./powerup-manager');
const coreHttpApi = require('../apis/core-http-api');
const { gameOver } = require('../controllers/game-controller');

// Global Constants
const CORE_GAME_ID = process.env.CORE_GAME_ID;

/**
 * * * * * * * The Maestro * * * * * * *
 *
 * This functions initializes clientsServer connections and the coreServer connections
 * Acts like a maestro, a conductor
 * @param {WebSocketWrapper} clientsServer
 * @param {WebSocketWrapper} coreServer
 */
module.exports.Maestro = function (clientsServer, coreServer) {
    // Module return object
    const obj = {};

    // Managers instances
    const clients = new clientsManager.Clients();
    const games = new gamesManager.Games(clients);
    const powerups = new powerupsManager.Powerups(clients);

    // Setting up core
    const requestId = util.uid();
    games.addGlobalVariable('CORE_SOCKET', coreServer);
    games.addGlobalVariable('REQUEST_ID', requestId);
    games.addGlobalVariable('GAME_ID', CORE_GAME_ID);

    /**
     * This functions are for development only.
     * Delete on actual production situation
     */
    obj.peekGames = games.peek;
    obj.peekClients = clients.peek;

    /**
     * Register handler for client: onConnection event
     */
    clientsServer.registerOnConnectionOpenHandler((client) => {
        const sessionId = client.sessionId;
        const newRequest = { sessionId: client.sessionId, module: client.module, params: client.params };

        util.writeLogText('ALL CLIENT KEYS: ' + JSON.stringify(clients.getRegisteredClients()));
        util.writeLogText('NEW CLIENT BROWSER CONNECTION: ' + JSON.stringify(newRequest));
        if (!clients.exists(client.sessionId)) playerController.echo.unauthorized(client.socket, 'Register first');
        if (clients.exists(sessionId)) {
            clients.setSocket(sessionId, client.socket);
            // Creates a function to use for client destruction
            const clientDestructionAction = function (coreSocket, message) {
                coreServer._send(JSON.stringify(message));
                client.socket.send(
                    JSON.stringify({ command: 'setConnectionTimeout', message: { milliseconds: config.server.clientTimeout } })
                );
                //clients.destroy(sessionId);
            };
            // Prepares message template for core on client destruction
            const destroyMessageToCoreServer = [{ event: 'destroy-client', 'session-id': sessionId }];
            clients.setTimeoutAction(
                sessionId,
                wsWrapper.destroySocketOntimeout(
                    client.socket,
                    config.server.clientTimeout,
                    clientDestructionAction,
                    destroyMessageToCoreServer
                )
            );
        }
    });

    // Test module are for websocket integration tests
    // Only runs when not in production
    if (process.env.NODE_ENV !== 'production') {
        // Tests
        clientsServer.registerOnMessageHandler({
            module: 'test',
            command: 'testWebsocketServer',
            action: (client, message) => {
                playerController.echo.ok(client.socket, message);
            },
        });
    }

    /**
     * Register player actions handlers for websocket onClientMessage events
     */
    clientsServer.commandBatchRegister('player', {
        /**
         * Trigger event on client message
         * Used for instance to renew timeout on each user interaction
         */
        '//ANY_COMMAND': (client, message) => {
            if (!clients.hasPlayer(client.sessionId)) playerController.echo.unauthorized(client.socket, message);
            else
                clients.setTimeoutAction(
                    client.sessionId,
                    wsWrapper.renewDestroySocketTimer(clients.getTimeoutAction(client.sessionId))
                );
        },
        /**
         * Register event handler to deal with player data requests
         */
        getPlayer: clients.sendPlayer,
        /**
         * Register client response handler
         */
        ok: games.handlePlayerResponse,
        /**
         * Start match, calls game manager function
         */
        startMatch: games.startMatch,
        /**
         * Players playes, calls game-manager function
         */
        play: games.play,
        /**
         * Players request store items and prices
         */
        getStore: powerups.sendStore,
        /**
         * Register event handler to deal with the a player move
         */
        buyPowerup: powerups.buyPowerup,
        /**
         * Consumes a powerup
         */
        usedPowerup: powerups.usedPowerup,
        /**
         * get user games Classufication
         */
        getClassify: games.getClassify,
        /**
         * get ranking the bests players
         */
        getRanking: clients.getRanking,
    });

    /**
     * Register developers accounts for tests
     */
    const devUsers = require('../devUsers.json');
    devUsers.forEach((record) => {
        clients.validateSessionId(
            record,
            record.name,
            (validationError, player) => {
                if (validationError) {
                    return; // console.log(validationError)
                }
                //console.log('Registered: ' + player.name);
            },
            true
        );
    });

    /**
     * Register onconnection event handler
     */
    coreServer.registerOnConnectionOpenHandler(async (wss) => {
        const coreApiResponse = await coreHttpApi.getNewCoreToken();
        if (!coreApiResponse || !coreApiResponse.success) {
            // do something
            // console.log('Failed connecting to Core :(');
            util.writeLogText('Failed connecting to Core :(');
        } else {
            const handshake = {
                event: 'handshake',
                'request-id': requestId,
                data: {
                    token: coreApiResponse.token,
                    'game-id': CORE_GAME_ID,
                },
            };
            wss.send(JSON.stringify(handshake));
            util.writeLogText('Successfully connected to Core :)');
            //console.log('Successfully connected to Core :)');
        }
    });

    /**
     * Register core server success message
     */
    coreServer.registerOnMessageHandler({
        event: 'success',
        action: (wss, message) => {
            // TODO if needed any action on default core success message
        },
    });

    /**
     * Register core server start-match event
     */
    coreServer.registerOnMessageHandler({
        event: 'start-match',
        action: (wss, message) => {
            const sessionId = message.data['session-id'];
            const response = { event: 'success', 'request-id': message['request-id'] };

            util.writeLogText(JSON.stringify(message));
            clients.validateSessionId(message.data.user, sessionId, (validationError, player) => {
                if (validationError) {
                    response.event = 'error';
                }
                return wss.send(JSON.stringify(response));
            });
        },
    });

    return obj;
};
