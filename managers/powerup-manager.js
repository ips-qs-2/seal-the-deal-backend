/**
 * @module store-manager
 * Handles store items, prices and transactions
 *
 * @package managers
 *
 * @returns following methods:
 * @public allPlayers
 *
 * @author Seal the Deal - Team
 * @version 1.0.1
 * @date 2020/11/28
 * @lastrevision
 *
 */

// Import modules
const playerController = require('../controllers/player-controller');

module.exports.Powerups = function (clients) {
    // Module return object
    const obj = {};

    /**
     * Provide store items and prices
     * @param {object} client
     * @param {object} message
     */
    obj.sendStore = (client, message) => {
        if (!clients.hasPlayer(client.sessionId)) {
            playerController.echo.unauthorized(client.socket, message);
        } else {
            playerController.setStore(client.socket, clients.getPlayer(client.sessionId));
            playerController.echo.ok(client.socket, message);
        }
    };

    /**
     * Executes transaction, credits for porwerups
     * @param {object} client
     * @param {object} message
     */
    obj.buyPowerup = (client, message) => {
        if (!clients.hasPlayer(client.sessionId)) {
            playerController.echo.unauthorizedEcho(client.socket, message);
        } else {
            playerController.buyPowerups(client.socket, clients.getPlayer(client.sessionId), message);
        }
    };

    /**
     * Consume powerup
     * @param {object} client
     * @param {object} message
     */
    obj.usedPowerup = (client, message) => {
        const player = clients.getPlayer(client.sessionId);
        playerController.updatePowerupState(client.socket, player, message);
    };

    return obj;
};
